# ArchLabs Installer

Shell script to install Arch Linux based distributions with a focus on customization

#### Features

- Minimal dependencies.
- LUKS and/or LVM support.
- Btrfs and subvolume support.
- Full device auto partitioning.
- Mirror selection and sorting.
- Self updating with persistent settings.
- Base packages install in the background for faster installs.
- Choose your own kernel, file system, bootloader, packages, sessions, shell, login manager, etc.


#### Requirements

- `awk`
- `sed`
- `curl`
- `bash`
- `dialog`
- `parted`
- `coreutils`
- `findutils`
- `util-linux`
- `arch-install-scripts`


#### Manual Installation

```
- boot with latest archlinux ISO
nano /etc/pacman.conf | color | parallel 10 | verbose | ILoveCandy
pacman-key --init
pacman-key --populate
pacman -Sy awk sed curl bash dialog parted coreutils findutils util-linux arch-install-scripts
curl -fSL https://gitlab.com/mauriicio/archlabs-installer/-/raw/master/installer2 -o /usr/local/bin/installer
chmod +x /usr/local/bin/installer
installer
```
